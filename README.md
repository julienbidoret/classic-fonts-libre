# Classic-Fonts-Libre

Classic-Fonts-Libre

## Garamond - 1540 
* EB-Garamond - Georg Duffner - 2010 -> [sources](https://github.com/georgd/EB-Garamond)
* Cormorant - Christian Thalmann - 2015 ->  [sources](https://github.com/CatharsisFonts/Cormorant)
* Garamond Libre - Daniel Benjamin Miller - 2019 -> [sources](https://github.com/dbenjaminmiller/garamond-libre.git)
* RW Garamond - Daniel Benjamin Miller- 2019 -> [source](https://github.com/dbenjaminmiller/rwgaramond)

## Caslon - 1734
* Libre Caslon Text - Pablo Impalari - 2012 -> [sources](https://github.com/impallari/Libre-Caslon-Text)
* Libre Caslon Display - Pablo Impalari - 2012 -> [sources](https://github.com/impallari/Libre-Caslon-Display)
* Career - Antoine Gelgon - 2015 -> [sources](https://gitlab.com/Luuse/foundry/Career)

## Baskerville - 1757
* Libre Baskerville - Pablo Impalari - 2012 -> [sources](https://github.com/impallari/Libre-Baskerville) 
* Baskervville - ANRT - 2017 -> [sources](https://github.com/anrt-type/ANRT-Baskervville)
* NX Baskerville - Daniel Benjamin Miller - 2020 -> [source](https://github.com/dbenjaminmiller/NXBaskerville)
* Baskervald ADF - Clea F. Rees - 2010 ->[source](https://www.ctan.org/pkg/baskervaldadf)
* Baskervville - Rosalie Wagner & ANRT - 2017 – 2020 ->[source](https://github.com/anrt-type/ANRT-Baskervville)

## Plantin - 1913
* Epistle - Elie Heuer - 2018 -> [sources](https://github.com/eliheuer/epistle)
* Adelphe - Eugénie Bidaut - 2022 -> [sources](https://typotheque.genderfluid.space/adelphe.html)

## Century Schoolbook - 1915
* Century Schoolbook L - Timothy Gu - ? -> [source](https://github.com/TimothyGu/Century-Schoolbook-L)
* TeX Gyre Schola - GUST - 2006 [source](https://www.gust.org.pl/projects/e-foundry/tex-gyre/schola)

## Times New Roman - 1932 
* FreeSerif - GNU - 2011 -> [sources](https://www.gnu.org/software/freefont/)
* Nimus Roman No. 9 L - URW++ - 1982 -> [source](https://www.fontsquirrel.com/fonts/list/foundry/urw)
* Tex-Gyre-Termes - GUST - 2006  - [source](https://www.ctan.org/pkg/tex-gyre-termes)

## Palatino - 1948
* URW Palladio - Walter Schmidt - 2004 -> [source](https://tug.org/FontCatalogue/urwpalladio/)
* Tex Gyre Pagella - Gust - 2008 -> [source](https://www.fontsquirrel.com/fonts/tex-gyre-pagella)

## Helvetica - 1957
* Nimbus Sans - URW++ - 1999 -> [source](https://www.fontsquirrel.com/fonts/nimbus-sans-l)

## Avant Garde - 1970
*  TeX Gyre Adventor - GUST - 2007 -> [source](https://www.gust.org.pl/projects/e-foundry/tex-gyre/adventor/index_html?searchterm=+TeX+Gyre+Adventor)
